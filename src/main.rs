#[macro_use]
extern crate lazy_static;

use clap::{App, Arg};
use futures::future::Future;
use rand::{thread_rng, Rng};
use rdkafka::config::ClientConfig;
use rdkafka::message::OwnedHeaders;
use rdkafka::producer::{FutureProducer, FutureRecord};
use regex::Regex;
use std::collections::HashSet;
use std::fs::File;
use std::io::{BufRead, BufReader, Error, ErrorKind};

pub trait GetRandom {
    type Item;
    fn get_random<R: Rng>(&self, rng: &mut R) -> Option<Self::Item>;
}

impl<T> GetRandom for Vec<T>
where
    T: Clone,
{
    type Item = T;
    fn get_random<R: Rng>(&self, rng: &mut R) -> Option<Self::Item> {
        if self.is_empty() {
            None
        } else {
            let index = rng.gen_range(0, self.len());
            Some(self[index].clone())
        }
    }
}

fn split_line<'a>(line: &'a str) -> impl Iterator<Item = String> + 'a {
    lazy_static! {
        static ref RE: Regex = Regex::new(r"[,\.!\?\(\)\\:;]").unwrap();
    }
    line.split_whitespace()
        .map(|w| w.to_lowercase())
        .map(|w| RE.replace_all(&w, "").into_owned())
}

fn get_words(filename: &str) -> Result<Vec<String>, ()> {
    File::open(filename)
        .and_then(|f| Ok(BufReader::new(f)))
        .and_then(|f| {
            f.lines()
                .map(|l| match l {
                    Ok(ref line) => Ok(split_line(line).collect::<Vec<String>>()),
                    Err(_) => Err("Can't read line!"),
                })
                .fold(Ok(HashSet::new()), |agg, words| match agg {
                    Ok(mut set) => match words {
                        Ok(w) => {
                            set.extend(w);
                            Ok(set)
                        }
                        Err(_) => Err(Error::from(ErrorKind::InvalidData)),
                    },
                    Err(_) => Err(Error::from(ErrorKind::InvalidData)),
                })
        })
        .and_then(|hs| {
            Ok(hs
                .iter()
                .map(|word| word.to_string())
                .collect::<Vec<String>>())
        })
        .map_err(|_| eprintln!("Couldn't open file!"))
}

fn main() {
    let matches = App::new("Arbitrary word producer")
        .version(option_env!("CARGO_PKG_VERSION").unwrap_or(""))
        .about("A simple Kafka producer which takes words from a file and outputs them in arbitrary order")
        .arg(Arg::with_name("brokers")
            .short("b")
            .long("brokers")
            .help("broker list in kafka format")
            .takes_value(true)
            .default_value("localhost:9092"))
        .arg(Arg::with_name("topic")
            .short("t")
            .long("topic")
            .help("destination topic")
            .takes_value(true)
            .required(true))
        .arg(Arg::with_name("word_file")
            .short("f")
            .long("file")
            .help("word file to be read from")
            .takes_value(true)
            .required(true))
        .arg(Arg::with_name("size")
            .short("s")
            .long("size")
            .help("number of sent messages")
            .takes_value(true)
            .default_value("10000"))
    .get_matches();

    let topic = matches.value_of("topic").unwrap();
    let brokers = matches.value_of("brokers").unwrap();
    let filename = matches.value_of("word_file").unwrap();
    let size = matches.value_of("size").unwrap().parse::<u32>().unwrap();

    if let Ok(words) = get_words(&filename) {
        let producer: FutureProducer = ClientConfig::new()
            .set("bootstrap.servers", brokers)
            .set("produce.offset.report", "true")
            .set("message.timeout.ms", "5000")
            .create()
            .expect("Producer creation error");

        let mut rng = thread_rng();

        let futures = (0..size)
            .map(|i| {
                let word = &words.get_random(&mut rng).unwrap();
                // println!("{}", word);
                producer
                    .send(
                        FutureRecord::to(topic)
                            .key(&word)
                            .payload(&word)
                            .headers(OwnedHeaders::new().add("header_key", "header_value")),
                        0,
                    )
                    .map(move |delivery_status| {
                        // This will be executed onw the result is received
                        println!("Delivery status for message {} received", i);
                        delivery_status
                    })
            })
            .collect::<Vec<_>>();

        for future in futures {
            println!("Future completed. Result: {:?}", future.wait());
        }
    }
}

FROM rust:1.33 AS build

RUN USER=root cargo new --bin arbitrary-words-producer
WORKDIR ./arbitrary-words-producer
COPY ./Cargo.* ./

# Preliminarily retrieve and compile dependencies independently of custom source code
RUN cargo build --release
RUN rm -r src ./target/release/deps/arbitrary_words_producer*

COPY ./src ./src

RUN cargo build --release


FROM rust:1.33

COPY --from=build /arbitrary-words-producer/target/release/arbitrary-words-producer .
ENTRYPOINT ["./arbitrary-words-producer"]

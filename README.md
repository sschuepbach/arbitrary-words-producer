# arbitrary-words-producer

Extracts words from a file and sends them to Kafka in an arbitrary order

## Usage

### Directly

```sh
arbitrary-words-producer \
  --brokers <brokers> \
  --size <size> \
  --topic <topic> \
  --file <word_file>
```

### Via Docker

* Build docker image
```sh
docker build -tarbitrary-words-producer .
```
* Run
```sh
docker run --net=<network> -v/path/to/textfile:/textfile arbitrary-word-producer --file /textfile <other_app_parameters>
```
